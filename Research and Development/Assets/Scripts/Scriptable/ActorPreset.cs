using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "Actor", menuName = "ScriptableObjects/Actor", order = 1)]
public class ActorPreset : ScriptableObject
{
    [TableList] public Resource[] BaseNeeds; //Are required to start production
    [TableList] public Resource[] EfficiencyNeeds; //Are required to improve production efficiency
    public float EfficiencyBonus = 1;
    [TableList] public Resource[] LuxuryNeeds; //Are what the actor spends excess money on
    [TableList] public Resource[] Produces;
    [TableList] public Resource[] Storage = new Resource[1] {
        new Resource(ResourceType.Credits, 10, 1)
    };
    /// <summary>
    /// The percentage of its current wealth its willing to pay for the resources it needs.
    /// If the amount requested is lower than this it will use this percentage of its wealth instead
    /// </summary>
    [Range(0,1)]
    public float Generosity;

    public Actor CreateActor(Market market)
    {
        return new Actor(this, market);
    }
}
