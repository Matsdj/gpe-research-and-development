using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Buildable", menuName = "ScriptableObjects/Buildable/Basic", order = 1)]
public class Buildable : ScriptableObject
{
    [TableList]
    public Resource[] Costs;
    [TableList]
    public Resource[] Production;
    [TableList]
    public Resource[] Maintenance;
}
