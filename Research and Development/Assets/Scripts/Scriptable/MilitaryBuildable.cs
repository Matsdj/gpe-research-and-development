using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Buildable", menuName = "ScriptableObjects/Buildable/Military", order = 1)]
public class MilitaryBuildable : Buildable
{
    public int MilitaryPower;
}
