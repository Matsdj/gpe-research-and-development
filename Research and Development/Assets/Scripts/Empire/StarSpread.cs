using UnityEngine;

public class StarSpread : MonoBehaviour
{
    public enum GenType
    {
        BasicRandom,
        Perlin,
        RandomGrid
    }
    public GameObject starPrefab;
    public GenType genType;

    //Basic Random
    public Transform spawnPos;
    public float maxRadius;
    public float maxRotationStepSize;
    public int starCount;
    //Perlin

    //Grid
    public Vector2Int gridSize;
    public float spacing;
    public float maxRandomOffset;

    void Start()
    {
        switch (genType)
        {
            case GenType.BasicRandom:
                for (int i = 0; i < starCount; i++)
                {
                    float rotation = Random.Range(0, maxRotationStepSize);
                    transform.rotation = Quaternion.Euler(0, rotation, 0);
                    spawnPos.position = new Vector3(Random.Range(1, 1 + maxRadius), 0, 0);
                    GameObject star = Instantiate(starPrefab, spawnPos.position, Quaternion.identity, transform);
                }
                break;
            case GenType.Perlin:
                //Mathf.PerlinNoise();
                break;
            case GenType.RandomGrid:
                for (int z = 0; z < gridSize.x; z++)
                    for (int x = 0; x < gridSize.y; x++)
                    {
                        Vector3 offset = new Vector3(Random.Range(-maxRandomOffset, maxRandomOffset), Random.Range(-maxRandomOffset, maxRandomOffset), Random.Range(-maxRandomOffset, maxRandomOffset));
                        Instantiate(starPrefab, new Vector3(x * spacing, Random.Range(-spacing, spacing), z * spacing) + offset, Quaternion.identity, transform);
                    }
                break;
        }
    }
}
