using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public enum GoalTypes
{
    Economy,
    Military,
    Technology
}

public enum GoalWeightChangers
{
    LowResources, // Increase Economy Priority
    NearingDeficite, // Additional Economy Priority
    PreparingWar, // Increase Economy Priority
    War //Increase Military priority
}

public class AI : MonoBehaviour
{
    [TableList] public Resource[] EmpireResources;
    [SerializeField, TableList] private BuildingCount[] buildings;
    [SerializeField] private GoalTypes currentGoal;
    [SerializeField, TableList] private GoalWeight[] goals;
    [SerializeField] private int planningTime; //How far does it look ahead
    [SerializeField] private int resourceBufferSize; //When it goes below this economy priority increases
    [SerializeField] private bool atWar;
    [SerializeField] private float updateDelay;
    private float delay;

    void Start()
    {
        delay = updateDelay;
    }
#if UNITY_EDITOR
    [Button]
    public void ResetResources()
    {
        EmpireResources = new Resource[Enum.GetValues(typeof(ResourceType)).Length];
        for (int i = 0; i < EmpireResources.Length; i++)
        {
            EmpireResources[i] = new Resource((ResourceType)i, 1000, 1);
        }
    }
    [Button]
    public void ResetBuildables()
    {
        Buildable[] buildables = FindAssetsByType<Buildable>().ToArray();
        buildings = new BuildingCount[buildables.Length];
        for (int i = 0; i < buildables.Length; i++)
        {
            buildings[i] = new BuildingCount(buildables[i]);
        }
    }
    [Button]
    public void ResetGoals()
    {
        goals = new GoalWeight[Enum.GetValues(typeof(GoalTypes)).Length];
        foreach (GoalTypes type in Enum.GetValues(typeof(GoalTypes)).Cast<GoalTypes>())
        {
            goals[(int)type] = new GoalWeight(type);
        }
    }
    [Button]
    public void ResetOthers()
    {
        planningTime = 60;
        atWar = false;
        updateDelay = 1;
    }
    public static List<T> FindAssetsByType<T>() where T : UnityEngine.Object
    {
        List<T> assets = new List<T>();
        string[] guids = AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T)));
        for (int i = 0; i < guids.Length; i++)
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
            T asset = AssetDatabase.LoadAssetAtPath<T>(assetPath);
            if (asset != null)
            {
                assets.Add(asset);
            }
        }
        return assets;
    }
#endif

    void Update()
    {
        delay -= Time.deltaTime;
        if (delay <= 0)
        {
            delay = updateDelay;
            float[] resourcesIncrease = Income();
            DecideGoal(resourcesIncrease);
            switch (currentGoal)
            {
                case GoalTypes.Economy:
                    //Build whatever is most in the negative
                    Resource lowestResource = EmpireResources[0];
                    for (int i = 1; i < EmpireResources.Length; i++)
                    {
                        if (EmpireResources[i].Amount < lowestResource.Amount) lowestResource = EmpireResources[i];
                    }
                    //Build first building that produces that resource
                    for (int i = 0; i < buildings.Length; i++)
                    {
                        bool build = false;
                        foreach(Resource resource in buildings[i].Building.Production)
                        {
                            if (resource.Type == lowestResource.Type)
                            {
                                Build(i);
                                build = true;
                                break;
                            }
                        }
                        if (build) break;
                    }
                    Debug.Log("Didn't build anything");
                    break;
                case GoalTypes.Military:
                    //Build first Military thing it finds
                    for (int i = 0; i < buildings.Length; i++)
                    {
                        BuildingCount building = buildings[i];
                        if (building.Building.GetType() == typeof(MilitaryBuildable))
                        {
                            Build(i);
                            break;
                        }
                    }
                    break;
                case GoalTypes.Technology:
                    break;
            }
        }
    }

    private float[] Income()
    {
        float[] resourcesIncrease = new float[EmpireResources.Length];
        foreach (BuildingCount bc in buildings)
        {
            void CheckResource(Resource resource, float value)
            {
                EmpireResources[GetIndex(resource.Type)].Amount += value;
                resourcesIncrease[GetIndex(resource.Type)] += value;
            }
            //Production
            foreach (Resource resource in bc.Building.Production)
            {
                CheckResource(resource, resource.Amount * bc.Count);
            }
            //Maintenance
            foreach (Resource resource in bc.Building.Maintenance)
            {
                CheckResource(resource, -(resource.Amount * bc.Count));
            }
        }
        return resourcesIncrease;
    }

    private int GetIndex(ResourceType resourceType)
    {
        int index = 0;
        //This switch statement is a quick fix to make sure it still works in newer versions
        switch (resourceType)
        {
            case ResourceType.Credits:
                index = 0;
                break;
            case ResourceType.Energy:
                index = 1;
                break;
            case ResourceType.Minerals:
                index = 2;
                break;
        }
        return index;
    }

    private void DecideGoal(float[] resourcesIncrease)
    {
        //Economy
        int nearDeficiteWeight = 0;
        int lowResourceWeight = 0;
        for (int i = 0; i < resourcesIncrease.Length; i++)
        {
            //Low resource
            if (EmpireResources[i].Amount < resourceBufferSize)
            {
                lowResourceWeight = 10;
            }
            //Deficite
            if (resourcesIncrease[i] >= 0) continue;
            else
            {
                float timeUntilDeficit = EmpireResources[i].Amount / -resourcesIncrease[i];
                if (timeUntilDeficit < planningTime)
                {
                    float weight = (planningTime - timeUntilDeficit) / planningTime * 10;
                    if (weight > nearDeficiteWeight) nearDeficiteWeight = Mathf.CeilToInt(weight);
                }
            }
        }

        goals[(int)GoalTypes.Economy].AddWeight(GoalWeightChangers.NearingDeficite, nearDeficiteWeight);
        goals[(int)GoalTypes.Economy].AddWeight(GoalWeightChangers.LowResources, lowResourceWeight);
        //Military
        int warWeight = 0;
        if (atWar) warWeight = 10;
        goals[(int)GoalTypes.Military].AddWeight(GoalWeightChangers.War, warWeight);

        //Final choice
        GoalWeight nextGoal = goals[0];
        for (int i = 1; i < goals.Length; i++)
        {
            GoalWeight goal = goals[i];
            if (nextGoal.TotalWeight < goal.TotalWeight) nextGoal = goal;
        }
        currentGoal = nextGoal.Type;
    }

    private void Build(int BuildingIndex)
    {
        //Check if sufficient resources
        foreach (Resource resource in buildings[BuildingIndex].Building.Costs)
        {
            if (!CheckBuyResource(resource)) return;
        }
        //Build
        buildings[BuildingIndex].Count++;
        foreach (Resource resource in buildings[BuildingIndex].Building.Costs)
        {
            EmpireResources[(int)resource.Type].Amount -= resource.Amount;
        }
    }

    private bool CheckBuyResource(Resource cost)
    {
        //For now a simple 1 to .5 conversion from resource to resource
        if (cost.Type == ResourceType.Credits && EmpireResources[(int)cost.Type].Amount < cost.Amount)
        {
            for (int i = 0; i < EmpireResources.Length; i++)
            {
                if (EmpireResources[i].Type == ResourceType.Credits) continue;
                if (EmpireResources[i].Amount >= cost.Amount * 2)
                {
                    EmpireResources[i].Amount -= cost.Amount * 2;
                    EmpireResources[GetIndex(cost.Type)].Amount += cost.Amount;
                    return true;
                }
            }
            return false;
        }
        else
        if (EmpireResources[GetIndex(cost.Type)].Amount < cost.Amount)
        {
            if (EmpireResources[(int)ResourceType.Credits].Amount < cost.Amount) return false;
            EmpireResources[(int)ResourceType.Credits].Amount -= cost.Amount;
            EmpireResources[GetIndex(cost.Type)].Amount += cost.Amount;
            return true;
        }
        return true;
    }

    [Serializable]
    private class BuildingCount
    {
        public Buildable Building;
        public int Count;
        public BuildingCount(Buildable building, int count = 1)
        {
            Building = building;
            Count = count;
        }
    }

    [Serializable]
    private class GoalWeight
    {
        public GoalTypes Type;
        public int BaseWeight = 1;
        public Dictionary<GoalWeightChangers, int> AddedWeights;
        public GoalWeight(GoalTypes type)
        {
            Type = type;
        }
        public int TotalWeight
        {
            get
            {
                int weight = BaseWeight;
                if (AddedWeights == null) AddedWeights = new Dictionary<GoalWeightChangers, int>();
                foreach (int addweight in AddedWeights.Values)
                {
                    weight += addweight;
                }
                return weight;
            }
        }
        public void AddWeight(GoalWeightChangers goal, int amount)
        {
            if (AddedWeights == null) AddedWeights = new Dictionary<GoalWeightChangers, int>();
            if (AddedWeights.ContainsKey(goal)) AddedWeights[goal] = amount;
            else AddedWeights.Add(goal, amount);
        }
    }
}