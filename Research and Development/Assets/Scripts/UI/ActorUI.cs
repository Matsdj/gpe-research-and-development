using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ActorUI : MonoBehaviour
{
    public TMP_Text ActorName;
    public ResourceUI ResourceUIPrefab;
    //private Actor actor;
    //private List<ResourceUI> actorResources = new List<ResourceUI>();
    public void SetActor(Actor actor, Actor player)
    {
        ActorName.text = actor.Name;
        //this.actor = actor;

        ////Clear old Resources
        //foreach (ResourceUI ui in actorResources)
        //{
        //    Destroy(ui.gameObject);
        //}
        //actorResources.Clear();

        foreach (Resource resource in actor.Storage.Values)
        {
            if (resource.Type == ResourceType.Credits || 
                resource.Type == ResourceType.Unfullfilled ||
                resource.Type == ResourceType.Fullfilled ||
                resource.Type == ResourceType.Trade ||
                resource.Amount <= 0) continue;

            Instantiate(ResourceUIPrefab, transform).SetResource(resource, actor, player, true, true, false);
        }
    }
}
