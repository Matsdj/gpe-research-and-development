using TMPro;
using UnityEngine;

public class ResourceUI : MonoBehaviour
{
    public TMP_Text ResourceName;
    public TMP_Text ResourceValue;
    public TMP_Text ResourceAmount;
    public GameObject BuyButton;
    public GameObject SellButton;
    private Resource product;
    private Actor productOwner;
    private Actor buyer;
    private SellUI sellUI;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="resource"></param>
    /// <param name="owner"></param>
    /// <param name="buyer">This is the player</param>
    /// <param name="useBuy"></param>
    /// <param name="showValue"></param>
    /// <param name="showAmount"></param>
    /// <param name="sellUI"></param>
    public void SetResource(Resource resource, Actor owner, Actor buyer, bool useBuy, bool showValue, bool showAmount, SellUI sellUI = null)
    {
        ResourceName.text = resource.Name;
        ResourceValue.text = resource.ExpectedValue.ToString();
        ResourceValue.gameObject.SetActive(showValue);
        ResourceAmount.text = resource.Amount.ToString();
        ResourceAmount.gameObject.SetActive(showAmount);
        product = resource;
        productOwner = owner;
        this.buyer = buyer;
        BuyButton.SetActive(useBuy);
        SellButton.SetActive(!useBuy);
        this.sellUI = sellUI;
    }

    public void Buy()
    {
        int amount = 1;
        if (product.Amount > 0)
        {
            Actor.Need need = new Actor.Need(product.Copy());
            need.Amount = amount;
            Actor.BuyNeed buyNeed = new Actor.BuyNeed(product, productOwner, need);
            Resource.AddToDictionary(buyer.Storage, need);
            buyer.Buy(buyNeed, 0);
        }
        else gameObject.SetActive(false);
    }

    public void Sell()
    {
        sellUI.UpdateUI(product);
    }
}
