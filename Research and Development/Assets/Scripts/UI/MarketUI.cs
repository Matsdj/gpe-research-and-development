using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Timeline;

public class MarketUI : MonoBehaviour
{
    public ActorUI ActorUIPrefab;
    public TMP_Dropdown MarketDropdown;
    private List<ActorUI> actors = new List<ActorUI>();

    private void Start()
    {
        MarketDropdown.onValueChanged.AddListener(OnMarketChange);
        MarketDropdown.ClearOptions();
        List<string> marketNames = new List<string>();
        //marketNames.Add("AllMarkets");
        foreach (Market market in MarketManager.instance.markets)
        {
            marketNames.Add(market.name);
        }
        MarketDropdown.AddOptions(marketNames);
        MarketManager.instance.OnAllCycleFinish.AddListener(UpdateMarketUI);
    }

    private void OnMarketChange(int i)
    {
        Market market = MarketManager.instance.FindMarket(i);
        CreateActors(market, Player.Instance.Actor);
    }

    public void UpdateMarketUI()
    {
        OnMarketChange(MarketDropdown.value);
    }

    public void CreateActors(Market market, Actor player)
    {
        //Clear old Actors
        foreach (ActorUI ui in actors)
        {
            Destroy(ui.gameObject);
        }
        actors.Clear();

        foreach (Actor actor in market.Actors)
        {
            //if (actor.Name == "PlayerStorage") continue;

            ActorUI ui = Instantiate(ActorUIPrefab, transform);
            ui.SetActor(actor, player);
            actors.Add(ui);
        }
    }
}
