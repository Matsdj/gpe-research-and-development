using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using static MarketHistory;
using static UnityEditor.Progress;

public class Options : MonoBehaviour
{
    public MarketManager MarketManager;
    public TMP_Dropdown Markets;
    public MarketHistory MarketHistory;
    public TMP_Dropdown Actors;
    public TMP_Dropdown Resources;
    public TMP_Dropdown Specifics;
    public TMP_InputField ValueCount;
    public TMP_InputField RangeMin;
    public TMP_InputField RangeMax;
    public GameObject barPrefab;
    public RectTransform barParent;
    private List<Bar> bars = new List<Bar>();

    private void Awake()
    {
        GenerateOptions();
        Markets.onValueChanged.AddListener(UpdateBars);
        Actors.onValueChanged.AddListener(UpdateBars);
        Resources.onValueChanged.AddListener(UpdateBars);
        Specifics.onValueChanged.AddListener(UpdateBars);
        ValueCount.onValueChanged.AddListener(UpdateBars);
        RangeMin.onValueChanged.AddListener(UpdateBars);
        RangeMax.onValueChanged.AddListener(UpdateBars);

        MarketManager.instance.OnAllCycleFinish.AddListener(UpdateBars);
    }

    [Button]
    void GenerateOptions()
    {
        Markets.ClearOptions();
        string[] marketNames = new string[MarketManager.markets.Length];
        foreach (Market market in MarketManager.markets)
        {
            marketNames[market.Id] = market.name;
        }
        List<string> options = new List<string>();
        options.AddRange(marketNames);
        Markets.AddOptions(options);

        Actors.ClearOptions();
        List<string> actorNames = new List<string>();
        actorNames.Add("All Actors");
        foreach (ActorPreset actor in MarketManager.actorPresets)
        {
            actorNames.Add(actor.name);
        }
        Actors.AddOptions(actorNames);

        Resources.ClearOptions();
        List<string> resourceNames = new List<string>();
        for (int r = 0; r < Resource.ResourceTypeCount; r++)
        {
            resourceNames.Add(Enum.GetName(typeof(ResourceType), r));
        }
        Resources.AddOptions(resourceNames);
    }

    public void UpdateBars()
    {
        UpdateBars(-1);
    }

    public void UpdateBars(int dropdownValue)
    {
        //Get dropdown values
        int marketId = Markets.value;
        int actor = Actors.value;
        int type = Resources.value;
        int specifics = Specifics.value;
        //Get History
        List<ResourceCycle> values;
        if (actor == 0) values = MarketHistory.GetValues(marketId, type);
        else
        {
            values = MarketHistory.GetValuesPerActorType(marketId, MarketManager.instance.actorPresets[actor-1].name, type);
        }
        
        //Clear previous bars
        foreach (Bar bar in bars)
        {
            Destroy(bar.gameObject);
        }
        bars.Clear();

        if (values == null) return;
        //Range
        int min = ParseInput(RangeMin, 0);
        int max = ParseInput(RangeMax, -1);
        if (max < 0 || max > values.Count-1) max = values.Count-1;
        //Modulus
        int modulus = Mathf.RoundToInt((float)(max-min+1) / ParseInput(ValueCount, 1));
        if (modulus <= 0) modulus = 1;

        float biggestValue = 0;
        foreach (ResourceCycle resource in values)
        {
            if (resource.Cycle < min) continue;
            if (resource.Cycle > max) break;
            if (resource.Cycle % modulus != 0 && resource.Cycle != max && resource.Cycle != min) continue;
            Bar bar = Instantiate(barPrefab, barParent.transform).GetComponent<Bar>();
            switch (specifics)
            {
                case 0:
                    bar.Value = resource.ExpectedValue;
                    break;
                case 1:
                    bar.Value = resource.Amount;
                    break;
                default:
                    Debug.LogError("Wrong Specifics Value");
                    break;
            }
            bar.timeText.text = resource.Cycle.ToString();
            if (bar.Value > biggestValue) biggestValue = bar.Value;
            bars.Add(bar);
        }
        foreach (Bar bar in bars)
        {
            float height = barParent.rect.height * ((bar.Value + 0.1f) / (biggestValue + 0.1f));
            //Debug.Log($"{barParent.rect.height} * ({bar.Value} / {biggestValue}) = {height}");
            bar.rectTransform.sizeDelta = new Vector2(100, height);
        }
    }
    public void UpdateBars(string s)
    {
        UpdateBars();
    }

    public int ParseInput(TMP_InputField inputField, int defaultValue)
    {
        int value = defaultValue;
        if (int.TryParse(inputField.text, out int v)) value = v;
        return value;
    }
}
