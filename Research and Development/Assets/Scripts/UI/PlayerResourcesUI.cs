using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerResourcesUI : MonoBehaviour
{
    public ResourceUI ResourceUIPrefab;
    public SellUI SellUI;
    private List<ResourceUI> resources;

    private void Start()
    {
        resources = new List<ResourceUI>();
        Player.Instance.Actor.OnBuy.AddListener(UpdateVisual);
        MarketManager.instance.OnAllCycleFinish.AddListener(UpdateVisual);
    }

    public void UpdateVisual()
    {
        //Clear old Actors
        foreach (ResourceUI ui in resources)
        {
            Destroy(ui.gameObject);
        }
        resources.Clear();

        foreach (Resource resource in Player.Instance.Actor.Storage.Values)
        {
            if (resource.Amount <= 0) continue;

            ResourceUI ui = Instantiate(ResourceUIPrefab, transform);
            resources.Add(ui);
            ui.SetResource(resource, Player.Instance.Actor, null, false, false, true, SellUI);
        }
    }
}
