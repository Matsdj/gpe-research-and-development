using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Bar : MonoBehaviour
{
    public RectTransform rectTransform;
    public Image image;
    [SerializeField] private TMP_Text valueText;
    private float value;
    public TMP_Text timeText;

    public float Value
    {
        get { return value; }
        set
        {
            valueText.text = value.ToString();
            if (valueText.text.Length > 4) valueText.text = valueText.text.Remove(4);
            this.value = value;

            if (value == 0) valueText.gameObject.SetActive(false);
            else valueText.gameObject.SetActive(true);
        }
    }
}
