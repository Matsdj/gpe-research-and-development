using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SellUI : MonoBehaviour
{
    public TMP_Text ResourceName;
    public TMP_Text MarketName;
    public TMP_InputField ResourceValue;
    public TMP_InputField ResourceAmount;
    public Button SellButton;
    public TMP_Dropdown markets;
    private Resource resource;

    public void Awake()
    {
        SellButton.onClick.AddListener(Sell);
    }
    public void UpdateUI(Resource resource)
    {
        this.resource = resource;
        ResourceName.text = resource.Name;
        MarketName.text = MarketManager.instance.FindMarket(markets.value).name;
        gameObject.SetActive(true);
    }
    public void Sell()
    {
        Resource product = resource.Copy();
        product.ExpectedValue = float.Parse(ResourceValue.text);
        product.Amount = int.Parse(ResourceAmount.text);
        Resource.AddToDictionary(Player.Instance.Storages[markets.value].Storage, product);
        product.Amount = -int.Parse(ResourceAmount.text);
        Resource.AddToDictionary(Player.Instance.Actor.Storage, product);
        
        gameObject.SetActive(false);
    }
}
