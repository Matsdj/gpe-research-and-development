using System;
using System.Collections.Generic;

public enum ResourceType
{
    Credits,
    Workforce,
    Food,
    Energy,
    Minerals,
    Trade, // is the amount of goods it can move
    Unfullfilled,
    Fullfilled
}

[Serializable]
public class Resource
{
    public ResourceType Type;
    public float Amount;
    /// <summary>This would be the value that the actor is used too</summary>
    public float ExpectedValue;
    public float TotalExpectedValue
    {
        get { return ExpectedValue * Amount; }
    }
    public string Name
    {
        get { return Enum.GetName(typeof(ResourceType), Type); }
    }
    public Resource(ResourceType type, float amount, float expectedValue)
    {
        Type = type;
        Amount = amount;
        ExpectedValue = expectedValue;
    }

    public Resource(Resource resource)
    {
        Type = resource.Type;
        Amount = resource.Amount;
        ExpectedValue = resource.ExpectedValue;
    }

    public Resource Copy()
    {
        return new Resource(Type, Amount, ExpectedValue);
    }

    public Resource Copy(float multiplyAmountBy)
    {
        return new Resource(Type, Amount * multiplyAmountBy, ExpectedValue);
    }

    public virtual void LowerExpected()
    {
        ExpectedValue /= 1.1f;
        if (ExpectedValue < 0.01f) ExpectedValue = 0.01f;
    }
    public virtual void IncreaseExpected()
    {
        ExpectedValue *= 1.1f;
        if (ExpectedValue > 999) ExpectedValue = 999;
    }

    public string StringOfValues { get { return $"Type:{Type}, Amount:{Amount}, ExpectedValue:{ExpectedValue}"; } }

    private static int resourceTypeCount = -1;
    public static int ResourceTypeCount
    {
        get
        {
            if (resourceTypeCount == -1) resourceTypeCount = Enum.GetValues(typeof(ResourceType)).Length;
            return resourceTypeCount;
        }
    }

    public static void SetInDictionary(IDictionary<ResourceType, Resource> dictionary, Resource resource)
    {
        if (dictionary.ContainsKey(resource.Type)) dictionary[resource.Type].Amount = resource.Amount;
        else dictionary.Add(resource.Type, resource.Copy());
    }

    public static void AddToDictionary(IDictionary<ResourceType, Resource> dictionary, Resource resource)
    {
        if (dictionary.ContainsKey(resource.Type)) dictionary[resource.Type].Amount += resource.Amount;
        else dictionary.Add(resource.Type, resource.Copy());
    }

    public static void AddToDictionary(IDictionary<ResourceType, Resource> dictionary, Resource resource, float multiplyAmountBy)
    {
        if (dictionary.ContainsKey(resource.Type)) dictionary[resource.Type].Amount += resource.Amount * multiplyAmountBy;
        else dictionary.Add(resource.Type, resource.Copy(multiplyAmountBy));
    }
}
