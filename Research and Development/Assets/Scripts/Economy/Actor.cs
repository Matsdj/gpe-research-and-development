using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UnityEngine;
using static Market;

/// <summary>
/// This is the most basic piece which the economy is build out of
/// </summary>
[Serializable]
public class Actor
{
    [TableList, SerializeField] protected NeedCollection BaseNeeds; //Base needs are required to start production
    [TableList, SerializeField] protected NeedCollection EfficiencyNeeds; //Are required to improve production efficiency
    protected float EfficiencyBonus; //A value that the production is multiplied by
    [TableList, SerializeField] protected Resource[] Products;
    public Dictionary<ResourceType, Resource> Storage;
    public string Name; //Mostly for UI purposes but also to combine values with the same name in the markethistory
    protected Market market;
    private bool produced;
    private MarketConnection tradeMarket;
    private float totalNeedCost;
    private float generosity;

    [TableList, SerializeField] protected List<Resource> DisplayStorage; //TEMP array so the storage is viewable in the inspector

    public Actor(ActorPreset preset, Market market)
    {
        Name = preset.name;
        BaseNeeds = new NeedCollection(preset.BaseNeeds, this);
        BaseNeeds.FullfilledNeeds = true;
        EfficiencyNeeds = new NeedCollection(preset.EfficiencyNeeds, this);
        EfficiencyBonus = preset.EfficiencyBonus;
        Products = new Resource[preset.Produces.Length];
        Storage = new Dictionary<ResourceType, Resource>();
        for (int i = 0; i < Products.Length || i < preset.Storage.Length; i++)
        {
            if (i < Products.Length) Products[i] = preset.Produces[i].Copy();
            if (i < preset.Storage.Length) Storage.Add(preset.Storage[i].Type, preset.Storage[i].Copy());
        }
        this.market = market;
        generosity = preset.Generosity;
    }

    public bool BuyNeeds(Actor[] actorsToBuyFrom, float taxes = 0)
    {
        bool boughtBaseNeeds = BaseNeeds.BuyNeeds(actorsToBuyFrom);
        if (boughtBaseNeeds && EfficiencyNeeds.Values.Length > 0) EfficiencyNeeds.BuyNeeds(actorsToBuyFrom);
        return boughtBaseNeeds;
    }

    public virtual void Buy(BuyNeed buyneed, float taxes)
    {
        Storage[ResourceType.Credits].Amount -= buyneed.Price;
        buyneed.ProductOwner.Storage[ResourceType.Credits].Amount += buyneed.Price;
        buyneed.Product.Amount -= buyneed.Need.Amount;
    }

    public struct BuyNeed
    {
        public Resource Product;
        public Actor ProductOwner;
        public Need Need;
        public float Price;
        public BuyNeed(Resource product, Actor productOwner, Need need)
        {
            Product = product;
            ProductOwner = productOwner;
            Need = need;
            Price = Mathf.Max(Product.ExpectedValue, Need.MinValue);
        }
    }

    public virtual void Produce(float importTax = 0)
    {
        produced = false;
        if (!BaseNeeds.FullfilledNeeds) return;
        foreach (Resource product in Products)
        {
            if (product.Type == ResourceType.Trade)
            {
                //This could create issues when traders trade more than one item
                if (!Trade((int)MathF.Floor(product.Amount), importTax)) return;
                continue;
            }
            if (EfficiencyNeeds.FullfilledNeeds)
            {
                Resource.AddToDictionary(Storage, product, EfficiencyBonus);
            } else
            Resource.AddToDictionary(Storage, product);
        }
        //Add a cap of 10 resources
        foreach(Resource resource in Storage.Values)
        {
            if (resource.Type == ResourceType.Credits) continue;
            if (resource.Amount > 10) resource.Amount = 10;
        }

        produced = true;
        BaseNeeds.FullfilledNeeds = false;
        EfficiencyNeeds.FullfilledNeeds = false;
    }

    public bool Trade(int AmountOfGoods, float importTax)
    {
        try
        {
            if (tradeMarket == null) tradeMarket = market.MarketConnections[UnityEngine.Random.Range(0, market.MarketConnections.Length)];
            int profitableGoodsBought = 0;
            float[] marketValues = MarketManager.instance.marketHistory.GetPreviousMarketValues(market.Id);
            if (marketValues == null) return false;
            foreach (Actor actor in tradeMarket.Market.Actors)
            {
                foreach (Resource resource in actor.Storage.Values)
                {
                    if (resource.ExpectedValue + totalNeedCost / AmountOfGoods < marketValues[(int)resource.Type])
                    {
                        Buy(new BuyNeed(resource, actor, new Need(resource)), importTax);
                        Resource.AddToDictionary(Storage, new Resource(resource.Type, 1, marketValues[(int)resource.Type]));
                        //Debug.Log($"Trader Bought:{resource.Type}, Expected profit:{marketValues[(int)resource.Type] - (resource.ExpectedValue + totalNeedCost)}");
                        profitableGoodsBought++;
                        Resource.AddToDictionary(Storage, new Resource(ResourceType.Trade, 1, 1));
                        if (profitableGoodsBought >= AmountOfGoods) return true;
                    }
                    else
                    {
                        //Debug.Log($"Trader didn't buy:{resource.Type}, local value{marketValues[(int)resource.Type]}, " +
                        //    $"foreign value:{resource.ExpectedValue}, Need Cost:{totalNeedCost}, " +
                        //    $"result profit:{marketValues[(int)resource.Type] - (resource.ExpectedValue + totalNeedCost)}");
                    }
                }
            }
            tradeMarket = null;
        }
        catch
        {

        }
        return false;
    }

    public virtual void Check()
    {
        BaseNeeds.Check();
        BaseNeeds.DivideBudgetPerNeed(Storage[ResourceType.Credits].Amount);
        EfficiencyNeeds.Check();
        EfficiencyNeeds.DivideBudgetPerNeed(Storage[ResourceType.Credits].Amount - BaseNeeds.TotalNeedCost);
        totalNeedCost = BaseNeeds.TotalNeedCost + EfficiencyNeeds.TotalNeedCost;

        if (BaseNeeds.FullfilledNeeds)
        {
            float fullfilledNeedAmount = 1;
            if (EfficiencyNeeds.FullfilledNeeds) fullfilledNeedAmount += .1f;
            Resource.SetInDictionary(Storage, new Resource(ResourceType.Fullfilled, fullfilledNeedAmount, 1));
            Resource.SetInDictionary(Storage, new Resource(ResourceType.Unfullfilled, 0, 1));
        }
        else
        {
            Resource.SetInDictionary(Storage, new Resource(ResourceType.Fullfilled, 0, 1));
            Resource.SetInDictionary(Storage, new Resource(ResourceType.Unfullfilled, 1, 1));
        }

        foreach (Resource resource in Storage.Values)
        {
            if (resource.Amount <= 0)
            {
                if (produced) resource.IncreaseExpected();
            }
            else resource.LowerExpected();
        }
    }

    [Serializable]
    public class Need : Resource
    {
        /// <summary>This would be the maximum price someone would buy something for
        /// This would be determined by how much the buyer is making</summary>
        public float MaxValue = 999;
        /// <summary> 
        /// This would be the minimum someone is willing to pay for something if it is lower than this amount it will pay extra.
        /// This value comes forth from generosity.
        /// </summary>
        public float MinValue = 0;
        public Need(Resource resource) : base(resource) { }

        public override void LowerExpected()
        {
            base.LowerExpected();
            if (ExpectedValue < MinValue && MinValue > 0.01f) ExpectedValue = MinValue;
        }

        public override void IncreaseExpected()
        {
            base.IncreaseExpected();
            if (ExpectedValue > MaxValue) ExpectedValue = MaxValue;
        }
    }
    [Serializable]
    public class NeedCollection
    {
        private Need[] values;
        private float totalNeedCost;
        private Actor owner;
        public bool FullfilledNeeds = false;
        [TableList, SerializeField] public Need[] Values => values;
        public float TotalNeedCost => totalNeedCost;
        public int Length => values.Length;

        public NeedCollection(int needCount, Actor owner)
        {
            values = new Need[needCount];
            totalNeedCost = 0;
            this.owner = owner;
        }

        public NeedCollection(Resource[] needs, Actor owner)
        {
            values = new Need[needs.Length];
            totalNeedCost = 0;
            for (int i = 0; i < needs.Length; i++) values[i] = new Need(needs[i].Copy());
            this.owner = owner;
        }

        private float CalculateTotalNeedCost()
        {
            totalNeedCost = 0;
            foreach (Need need in values)
            {
                totalNeedCost += need.TotalExpectedValue;
            }
            return totalNeedCost;
        }

        public void DivideBudgetPerNeed(float budget)
        {
            CalculateTotalNeedCost();
            foreach (Need need in values)
            {
                need.MinValue = need.ExpectedValue / totalNeedCost * budget * owner.generosity;
            }
            foreach (Need need in values)
            {
                need.MaxValue = need.ExpectedValue / totalNeedCost * budget;
            }
        }

        public bool BuyNeeds(Actor[] actorsToBuyFrom, float taxes = 0)
        {
            if (FullfilledNeeds || values.Length == 0) return true;
            BuyNeed[] buyNeeds = new BuyNeed[values.Length];
            FullfilledNeeds = true;
            for (int i = 0; i < values.Length; i++)
            {
                Need need = values[i];
                bool found = false;
                foreach (Actor actor in actorsToBuyFrom)
                {
                    if (actor == owner) continue;
                    if (actor.Storage.ContainsKey(need.Type) && actor.Storage[need.Type].Amount >= need.Amount)
                    {
                        float needCost = actor.Storage[need.Type].ExpectedValue;
                        if (need.ExpectedValue >= needCost)
                        {
                            buyNeeds[i] = new BuyNeed(actor.Storage[need.Type], actor, need);
                            found = true;
                            break;
                        }
                    }
                }
                if (!found)
                {
                    return FullfilledNeeds = false;
                }
            }

            if (FullfilledNeeds)
            {
                for (int i = 0; i < Length; i++)
                {
                    owner.Buy(buyNeeds[i], taxes);
                }
                //Debug.Log($"{owner.Name} did buy to fullfillneeds");
            }
            else
            {
                //Debug.Log($"{name} didn't buy to fullfillneeds");
            }
            return FullfilledNeeds;
        }

        public void Check()
        {
            foreach (Resource need in values)
            {
                if (FullfilledNeeds) need.LowerExpected();
                else need.IncreaseExpected();
            }
        }
    }
}
