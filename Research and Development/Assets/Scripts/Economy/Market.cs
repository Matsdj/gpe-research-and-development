using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Market : MonoBehaviour
{
    public ActorPresetAmount[] ActorPresets;
    public List<Actor> Actors;
    public MarketConnection[] MarketConnections;
    public int Id;
    void Awake()
    {
        for (int i = 0; i < ActorPresets.Length; i++)
        {
            for (int c = 0; c < ActorPresets[i].Amount; c++)
            {
                Actor actor = ActorPresets[i].ActorPreset.CreateActor(this);
                Actors.Add(actor);
            }
        }
    }

    public void Produce()
    {
        foreach (Actor actor in Actors)
        {
            actor.Produce();
        }
    }

    public void BuyNeeds(bool traderLessConnections)
    {
        List<Actor> unfullfilledActors = new List<Actor>();
        foreach (Actor actor in Actors)
        {
            if (!actor.BuyNeeds(Actors.ToArray())) unfullfilledActors.Add(actor);
        }
        if (!traderLessConnections) return;
        //Traderless Connected Markets
        foreach (MarketConnection market in MarketConnections)
        {
            foreach (Actor actor in unfullfilledActors)
            {
                actor.BuyNeeds(market.Market.Actors.ToArray(), market.Taxes);
            }
        }
    }

    public void Check()
    {
        foreach (Actor actor in Actors)
        {
            actor.Check();
        }
    }

    [Serializable]
    public class MarketConnection
    {
        public Market Market;
        public float Taxes;
    }
    [Serializable]
    public class ActorPresetAmount
    {
        public ActorPreset ActorPreset;
        public int Amount;
    }
}
