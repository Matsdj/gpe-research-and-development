using UnityEngine;
using UnityEngine.Events;

public class MarketManager : MonoBehaviour
{
    public static MarketManager instance;
    public Market[] markets;
    public ActorPreset[] actorPresets;
    public MarketHistory marketHistory;
    public Options options;
    public UnityEvent OnCycleFinish = new UnityEvent();
    public UnityEvent OnAllCycleFinish = new UnityEvent();
    [SerializeField] private float updateDelay;
    [SerializeField] private float updateCount;
    [SerializeField] private bool continuouslyUpdateVisual;
    [SerializeField] private bool traderLessMarketConnections;
    private float delay;

    void Awake()
    {
        delay = updateDelay;
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        for(int i = 0; i < markets.Length; i++)
        {
            markets[i].Id = i;
        }
    }

    void Update()
    {
        delay -= Time.deltaTime;
        if (delay <= 0 && updateCount > 0)
        {
            updateCount--;
            RunCycle();
        }
    }
    public void RunCycle()
    {
        delay = updateDelay;
        RnDGeneral.Shuffle(markets);
        foreach (Market market in markets)
        {
            RnDGeneral.Shuffle(market.Actors);
            market.Produce();
        }
        foreach (Market market in markets) market.BuyNeeds(traderLessMarketConnections);
        foreach (Market market in markets) market.Check();

        OnCycleFinish.Invoke();
        if (updateCount == 0) OnAllCycleFinish.Invoke();
    }

    public Market FindMarket(int MarketId)
    {
        foreach(Market market in markets)
        {
            if (market.Id == MarketId) return market;
        }
        return null;
    }
}
