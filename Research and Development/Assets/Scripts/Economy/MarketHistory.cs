using System.Collections.Generic;
using UnityEngine;

public class MarketHistory : MonoBehaviour
{
    private MarketValueHistory[] marketValueHistories; //Average
    private Dictionary<string, MarketValueHistory>[] marketValueHistoryPerActor;
    private int cycle = 0;

    void Start()
    {
        marketValueHistories = new MarketValueHistory[MarketManager.instance.markets.Length];
        marketValueHistoryPerActor = new Dictionary<string, MarketValueHistory>[MarketManager.instance.markets.Length];
        for (int i = 0; i < MarketManager.instance.markets.Length; i++)
        {
            marketValueHistories[i] = new MarketValueHistory();
            marketValueHistoryPerActor[i] = new Dictionary<string, MarketValueHistory>();
        }
        MarketManager.instance.OnCycleFinish.AddListener(UpdateHistory);
    }

    public void UpdateHistory()
    {
        foreach (Market market in MarketManager.instance.markets)
        {
            int[] resourceProducerCount = new int[Resource.ResourceTypeCount];
            float[] resourceAmountTotal = new float[Resource.ResourceTypeCount];
            float[] resourceValueTotal = new float[Resource.ResourceTypeCount];
            foreach (Actor actor in market.Actors)
            {
                if (!marketValueHistoryPerActor[market.Id].ContainsKey(actor.Name))
                    marketValueHistoryPerActor[market.Id].Add(actor.Name, new MarketValueHistory());
                foreach (Resource stored in actor.Storage.Values)
                {
                    resourceProducerCount[(int)stored.Type]++;
                    resourceValueTotal[(int)stored.Type] += stored.ExpectedValue;
                    resourceAmountTotal[(int)stored.Type] += stored.Amount;

                    Dictionary<ResourceType, List<ResourceCycle>> resources = marketValueHistoryPerActor[market.Id][actor.Name].resources;
                    if (!resources.ContainsKey(stored.Type))
                        resources.Add(stored.Type, new List<ResourceCycle>());
                    if (resources[stored.Type].Count > 0 && resources[stored.Type][resources[stored.Type].Count - 1].Cycle == cycle)
                    {
                        resources[stored.Type][resources[stored.Type].Count - 1].Amount += stored.Amount;
                    }
                    else
                        resources[stored.Type].Add(new ResourceCycle(stored.Copy(), cycle));
                }
            }

            for (int r = 0; r < Resource.ResourceTypeCount; r++)
            {
                float averageValue = 0;
                if (resourceProducerCount[r] > 0) averageValue = resourceValueTotal[r] / resourceProducerCount[r];
                if (!marketValueHistories[market.Id].resources.ContainsKey((ResourceType)r)) marketValueHistories[market.Id].resources.Add((ResourceType)r, new List<ResourceCycle>());
                marketValueHistories[market.Id].resources[(ResourceType)r].Add(
                        new ResourceCycle((ResourceType)r, resourceAmountTotal[r], averageValue, cycle));
            }
        }
        cycle++;
    }

    public List<ResourceCycle> GetValuesPerActorType(int marketId, string actorName, int typeId)
    {
        try
        {
            return marketValueHistoryPerActor[marketId][actorName].resources[(ResourceType)typeId];
        }
        catch
        {
            return null;
        }
    }

    public List<ResourceCycle> GetValues(int marketId, int typeId)
    {
        try
        {
            return marketValueHistories[marketId].resources[(ResourceType)typeId];
        }
        catch
        {
            return null;
        }
    }

    public float[] GetPreviousMarketValues(int marketId)
    {
        if (cycle == 0) return null;
        float[] previousMarketValues = new float[Resource.ResourceTypeCount];
        for (int i = 0; i < Resource.ResourceTypeCount; i++)
        {
            List<ResourceCycle> resources = marketValueHistories[marketId].resources[(ResourceType)i];
            if (resources.Count == 0) return null;
            previousMarketValues[i] = resources[resources.Count - 1].ExpectedValue;
        }
        return previousMarketValues;
    }

    public class MarketValueHistory
    {
        public Dictionary<ResourceType, List<ResourceCycle>> resources;
        public MarketValueHistory()
        {
            resources = new Dictionary<ResourceType, List<ResourceCycle>>();
            //for (int i = 0; i < resources.Length; i++)
            //{
            //    resources[i] = new List<ResourceCycle>();
            //}
        }
    }

    public class ResourceCycle : Resource
    {
        public int Cycle;

        public ResourceCycle(ResourceType type, float amount, float expectedValue, int cycle) : base(type, amount, expectedValue)
        {
            Cycle = cycle;
        }
        public ResourceCycle(Resource resource, int cycle) : base(resource)
        {
            Cycle = cycle;
        }
    }
}
