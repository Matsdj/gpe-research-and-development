using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    public static Player Instance;
    public ActorPreset PresetActor;
    public ActorPreset PresetStorage;
    public EventActor Actor;
    public StorageActor[] Storages;

    private void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(gameObject);

        Actor = new EventActor(PresetActor, null);
    }

    private void Start()
    {
        Storages = new StorageActor[MarketManager.instance.markets.Length];
        //string debug = $"Player.cs Start()\n" +
        //    $"1. Made an storage for markets: ";
        foreach (Market market in MarketManager.instance.markets)
        {
            //debug += market.name + ", ";
            StorageActor storage = new StorageActor(PresetStorage, market, Actor);
            Storages[market.Id] = storage;
            storage.Name += market.Id;
            market.Actors.Add(storage);
        }
        //debug += $"\n2. Check if storage actually got added for markets:";
        //foreach (Market market in MarketManager.instance.markets)
        //{
        //    debug += " " + market.name;
        //    if (market.Actors.Contains(Storages[market.Id])) debug += "(Found!)";
        //    else debug += "(NotFound!)";
        //}
        //Debug.Log(debug);
        //MarketManager.instance.OnCycleFinish.AddListener(OnCycleFinish);
    }

    public void OnCycleFinish()
    {
        //string debug = $"OnCycleFinish() checks.\n";
        //debug += $"Storages length:{Storages.Length}\n";
        //debug += $"Check if storage is still market:";
        //for (int i = 0; i < Storages.Length; i++)
        //{
        //    StorageActor actor = Storages[i];
        //    debug += " " + actor.Name;
        //    if (actor.Market.Actors.Contains(actor))
        //    {
        //        debug += "(Found!)";
        //    }
        //    else
        //    {
        //        actor.Market.Actors.Add(actor);
        //        debug += "(NotFound!)";
        //    }
        //}
        //Debug.Log(debug);
    }

    public class EventActor : Actor
    {
        public UnityEvent OnBuy = new UnityEvent();
        public EventActor(ActorPreset preset, Market market) : base(preset, market) { }
        public override void Buy(BuyNeed buyneed, float taxes)
        {
            base.Buy(buyneed, taxes);
            OnBuy.Invoke();
        }
    }

    public class StorageActor : Actor
    {
        private EventActor mainActor;
        public Market Market { get => market; set => market = value; }
        public StorageActor(ActorPreset preset, Market market, EventActor mainActor) : base(preset, market)
        {
            this.mainActor = mainActor;
        }

        public override void Check()
        {
            //Debug.Log($"Running Check for StorageActor:{Name} in market:{market.name}");
            DisplayStorage = new List<Resource>();
            foreach (Resource resource in Storage.Values)
            {
                DisplayStorage.Add(resource);
            }
            float creditAmount = Storage[ResourceType.Credits].Amount;
            if (creditAmount > 0)
            {
                //Debug.Log($"Recieved {creditAmount} credits from remote in market:{market.name}");
                mainActor.Storage[ResourceType.Credits].Amount += creditAmount;
                Storage[ResourceType.Credits].Amount = 0;
            }
        }
    }
}
